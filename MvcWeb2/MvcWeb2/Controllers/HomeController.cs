﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWeb2.Controllers
{
    public class HomeController : Controller
    {
        
        SakaryaSporEntities db = new SakaryaSporEntities();

        public ActionResult Giris()
        {
            return View();
        }

        public ActionResult Index()
        {
           
            return View();

        }

        public ActionResult About()
        {
            List<Hakkimizda> Hakkimizda = db.Hakkimizda.ToList();
            ViewBag.Hakkimizda = Hakkimizda;
            return View();
        }

        public ActionResult Contact()
        {
            List<Iletisim> iletisim = db.Iletisim.ToList() ;
            ViewBag.iletisimbilgi = iletisim;
            return View();
        }

        public ActionResult Haberler()
        {
            return View();
           
        }


        //[ChildActionOnly]
        //public ActionResult _Giris()
        //{
        //    var liste = db.Giris;
        //    return View(liste);
        //}


        [ChildActionOnly]
        public ActionResult _Slider()
        {
            var liste = db.Slider.Where(x => x.BaslangicTarih < DateTime.Now && x.BitisTarih > DateTime.Now).OrderByDescending(x => x.ID);
            return View(liste);
        }
        [ChildActionOnly]
        public ActionResult _HaberOzet()
        {
            var liste = db.HaberTamIcerik;
            return View(liste);
        }
        [ChildActionOnly]
        public ActionResult _Galeri()
        {
            var liste = db.FotoGaleri.Where(x => x.BaslangicTarih < DateTime.Now && x.BitisTarih > DateTime.Now).OrderByDescending(x => x.ID);
            return View(liste);
        }
        [ChildActionOnly]
        public ActionResult _Haberler()
        {
            var liste = db.HaberTamIcerik;
            return View(liste);
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }



    }
}