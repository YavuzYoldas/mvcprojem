﻿using MvcWeb2.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWeb2.Areas.Admin.Controllers
{
    [Authorize(Roles = "yonetici")]
    public class HaberController : Controller
    {
        SakaryaSporEntities db = new SakaryaSporEntities();
        // GET: Admin/Haber
        public ActionResult Index()
        {
            var Model = db.HaberTamIcerik.OrderBy(x => x.HaberID).ToList();
            return View(Model);
        }

        public ActionResult Add()
        {

            return View();
        }

        const string imageFolderPath = "/images/";
        public ActionResult AddHaber(HaberModel model)
        {

            if (ModelState.IsValid)
            {

                string fileName = string.Empty;

                if (model.Resim != null && model.Resim.ContentLength > 0)
                {
                    fileName = model.Resim.FileName;
                    var path = Path.Combine(Server.MapPath("~" + imageFolderPath), fileName);
                    model.Resim.SaveAs(path);
                }


                HaberTamIcerik haber = new HaberTamIcerik();
                haber.HaberBaslik = model.HaberBaslik;
                haber.Haber = model.Haber;
                haber.HaberOzet = model.HaberOzet;
                haber.BaslangicTarih = model.BaslangicTarih;
                haber.BitisTarih = model.BitisTarih;
                haber.link = model.link;
                haber.HaberFotoYol = imageFolderPath + fileName;

                db.HaberTamIcerik.Add(haber);
                db.SaveChanges();

            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var model = db.HaberTamIcerik.Find(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(HaberTamIcerik model)
        {
            if (ModelState.IsValid)
            {


                db.HaberTamIcerik.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }
            else
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.HaberTamIcerik.Find(id);
            return View(model);
        }


        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.HaberTamIcerik.Find(id);
            db.HaberTamIcerik.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }









    }
}