﻿using MvcWeb2.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWeb2.Areas.Admin.Controllers
{


    [Authorize(Roles ="yonetici")]
    
    public class SliderController : Controller
    {

        SakaryaSporEntities db = new SakaryaSporEntities();
        // GET: Admin/Slider
        public ActionResult Index()
        {

           var model = db.Slider.OrderBy(x=>x.ID).ToList();
            return View(model);
        }
        public ActionResult Add()
        {
            
            return View();
        }


        const string imageFolderPath = "/Content/sliderimages/";




        public ActionResult AddSlider(SliderModel model)
        {
            
            if(ModelState.IsValid)
            {

                string fileName = string.Empty;

                if (model.Resim !=null && model.Resim.ContentLength>0)
                {
                    fileName = model.Resim.FileName;
                    var path =Path.Combine(Server.MapPath("~"+imageFolderPath),fileName);
                    model.Resim.SaveAs(path);
                }

                
                Slider slider = new Slider();
                slider.BaslangicTarih = model.BaslangicTarih;
                slider.BitisTarih = model.BitisTarih;
                slider.SlideText = model.SlideText;
                slider.ResimYol = imageFolderPath+ fileName;

                db.Slider.Add(slider);
                db.SaveChanges();

            }
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var model = db.Slider.Find(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(Slider model)
        {
            if (ModelState.IsValid)
            {
                
                db.Slider.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }
            else
            {
                return View(model);
            }
           
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.Slider.Find(id);
            return View(model);
        }


        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Slider.Find(id);
            db.Slider.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }






    }
}