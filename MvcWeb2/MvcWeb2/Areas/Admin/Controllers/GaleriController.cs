﻿using MvcWeb2.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWeb2.Areas.Admin.Controllers
{
    [Authorize(Roles = "yonetici")]
    public class GaleriController : Controller
    {
        // GET: Admin/Galeri


       
        SakaryaSporEntities db = new SakaryaSporEntities();
            // GET: Admin/Slider
            public ActionResult Index()
            {

                var model = db.FotoGaleri.OrderBy(x => x.ID).ToList();
                return View(model);
            }
            public ActionResult Add()
            {

                return View();
            }


            const string imageFolderPath = "/images/";




            public ActionResult AddFoto(FotoGaleriModel model)
            {

                if (ModelState.IsValid)
                {

                    string fileName = string.Empty;

                    if (model.Resim != null && model.Resim.ContentLength > 0)
                    {
                        fileName = model.Resim.FileName;
                        var path = Path.Combine(Server.MapPath("~" + imageFolderPath), fileName);
                        model.Resim.SaveAs(path);
                    }


                    FotoGaleri galeri = new FotoGaleri();
                    galeri.BaslangicTarih= model.BaslangicTarih;
                    galeri.BitisTarih = model.BitisTarih;
                    galeri.FotoText = model.FotoText;
                    galeri.FotoYol = imageFolderPath + fileName;

                    db.FotoGaleri.Add(galeri);
                    db.SaveChanges();

                }

                return RedirectToAction("Index");
            }

            [HttpGet]
            public ActionResult Edit(int id)
            {

                var model = db.FotoGaleri.Find(id);
                return View(model);
            }


            [HttpPost]
            public ActionResult Edit(FotoGaleri model)
            {
                if (ModelState.IsValid)
                {


                    db.FotoGaleri.Attach(model);
                    db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }
                else
                {
                    return View(model);
                }

                return RedirectToAction("Index");
            }

            [HttpGet]
            public ActionResult Delete(int id)
            {
                var model = db.FotoGaleri.Find(id);
                return View(model);
            }


            [HttpGet]
            public ActionResult DeleteConfirm(int id)
            {
                var model = db.FotoGaleri.Find(id);
                db.FotoGaleri.Remove(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
}