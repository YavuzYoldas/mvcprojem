﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb2.Areas.Admin.Models
{
    public class HaberModel
    {

        public int HaberID { get; set; }
         
        public int link  { get; set; }

        public string HaberBaslik { get; set; }

        public string Haber { get; set; }

        public string HaberOzet { get; set; }

        public Nullable<System.DateTime> BaslangicTarih { get; set; }

        public Nullable<System.DateTime> BitisTarih { get; set; }

        public HttpPostedFileBase Resim { get; set; }


    }
}