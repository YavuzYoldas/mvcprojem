﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcWeb2.Areas.Admin.Models
{
    public class SliderModel
    {

        public int ID { get; set; }

        public string SlideText { get; set; }

        public Nullable<System.DateTime> BaslangicTarih { get; set; }

        public Nullable<System.DateTime> BitisTarih { get; set; }

        public HttpPostedFileBase Resim { get; set; }


    }
}