#ifndef BST_H
#define BST_H
#include<iostream>

class BSTDugum
{
public:
	int Veri;
	BSTDugum* pSol;
	BSTDugum* pSag;

	


	BSTDugum(int Veri);
	void Ekle(int item);
	void Cikar(int item, BSTDugum* ebeveyn=NULL);
	BSTDugum* Ara(int item);
	int MinGetir();
	~BSTDugum();
};
#endif   
